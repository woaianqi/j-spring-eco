import {
  Component,
  Value,
  BeanPostProcessor,
  BeanDefine,
  methodAnnotationGenerator,
  Autowired,
  spring,
} from '../src';

/***
 * 测试多态继承
 *
 */
describe('测试多态继承，是否继承了属性和方法', () => {
  //创建大写注解
  const Upper = () => methodAnnotationGenerator('j-spring.Upper', {}, Upper);

  //创建方法返回变成大写增强 后置处理器
  @Component()
  class MsgUpperProxy implements BeanPostProcessor {
    getSort(): number {
      return 100;
    }
    postProcessBeforeInitialization(bean: any, beanDefine: BeanDefine): Object {
      beanDefine.methodList.forEach(md => {
        //方法大写增强
        if (md.hasAnnotation(Upper)) {
          bean[md.name] = new Proxy(bean[md.name], {
            apply(target, thisArg, argArray) {
              const reuslt = target.apply(thisArg, argArray) as string;
              return reuslt.toUpperCase();
            },
          });
        }
      });

      return bean;
    }
    postProcessAfterInitialization(bean: any, _beanDefine: BeanDefine): Object {
      return bean;
    }
  }

  abstract class Person {
    @Value({ path: 'school' })
    school: string;

    @Upper()
    public say(): string {
      return 'hello';
    }

    abstract getName(): string;

    getMsg(): string {
      return `${this.school}=>${this.getName()}:${this.say()}`;
    }
  }

  @Component()
  class Student extends Person {
    getName(): string {
      return '小明';
    }
  }

  @Component()
  class Teacher extends Person {
    getName(): string {
      return '小王';
    }
  }

  @Component()
  class Application {
    @Autowired()
    student: Student;

    @Autowired()
    teacher: Teacher;

    main() {
      return `${this.teacher.getMsg()},${this.student.getMsg()}`;
    }
  }

  it(`测试程序正常运行`, () => {
    const msg = spring
      .loadConfig({ school: '68中', 'j-spring.log.level': 'off' })
      .bindList([MsgUpperProxy, Student, Teacher, Application])
      .launch(Application);
    expect(msg).toEqual(`68中=>小王:HELLO,68中=>小明:HELLO`);
  });
});
