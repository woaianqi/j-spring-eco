import { Result, delay } from '../src';

describe('test result is ok', () => {
  it('test delay func', async () => {
    const a1 = new Date().getTime();
    await delay(2000);
    const a2 = new Date().getTime();
    expect(a2 - a1 >= 2000).toBe(true);
  });

  it(`sample use`, () => {
    const r = Result.of(123)
      .map(s => (s += 1))
      .get();

    expect(r).toBe(124);
  });

  it(`test string value`, () => {
    Result.of(`abc`)
      .map(s => s.toUpperCase())
      .ifPresent(s => {
        expect(s).toEqual(`ABC`);
      });
  });
});
