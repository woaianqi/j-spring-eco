import { Autowired, Component, Value, spring } from '../src';
import path from 'path';
spring.closeLog();

type Hook = {
  'pre-commit': string;
};

//测试配置文件 是否正确
describe('测试配置文件是否加载正确', () => {
  it('测试配置文件的读取', () => {
    @Component()
    class YamlConfig {
      @Value({ path: 'project.name', remark: 'the name of this project' })
      projectName: string;
    }

    @Component()
    class JsonConfig {
      @Value({ path: 'license', remark: 'spring license!' })
      license: string;

      @Value({ path: 'engines', remark: 'node version' })
      engines: {
        node: string;
      };

      @Value({ path: 'husky.hooks', remark: 'the husky hook' })
      hook: Hook;
    }

    @Component()
    class Application {
      @Autowired()
      yamlConfig: YamlConfig;

      @Autowired()
      jsonConfig: JsonConfig;

      main() {
        expect(
          `the project:${this.yamlConfig.projectName} license is ${this.jsonConfig.license}`
        ).toEqual(`the project:j-spring license is MIT`);
      }
    }

    spring
      .loadConfigFile('./package.json')
      .loadConfigFile(path.join(__dirname, 'config', 'configTest.yaml'))
      .launch(Application);
  });
});
