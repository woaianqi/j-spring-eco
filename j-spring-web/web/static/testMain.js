
doTest(1,{
    title:'测试整型的参数获取',
    req:()=> Get('/getTestApi/getParam1',{a:1,b:2}),
    assert:d => d===3
})

doTest(2,{
    title:'测试字符串类型的参数获取',
    req:()=> Get('/getTestApi/getParam2',{a:1,b:2}),
    assert:d => d==='12'
})

doTest(3,{
    title:'测试混合类型的参数获取',
    req:()=> Get('/getTestApi/getParam3',{a:1,b:2}),
    assert:d => d==='22'
})

doTest(4,{
    title:'测试数组字符串类型的传入',
    req:()=> Get('/getTestApi/getParam4',{a:`1,2,3`,b:'x'}),
    assert:d => d==='1-2-3-x'
})

doTest(5,{
    title:'测试resful传值',
    req:()=> Get('/getTestApi/getParam5/100/hello',{}),
    assert:d => d==='200-hello'
})

doTest(6,{
    title:'测试resful与query混合传值',
    req:()=> Get('/getTestApi/getParam6/100',{b:'hello'}),
    assert:d => d==='200-hello'
})

doTest(7,{
    title:'测试post请求简单携带参数',
    req:()=> Post('/PostTestApi/postParam1',{a:'2',b:'1'}),
    assert:d => d===3
})

doTest(8,{
    title:'测试混合类型的参数获取',
    req:()=> Post('/PostTestApi/postParam2',{a:'2',b:'1'}),
    assert:d => d===`3-1`
})


doTest(9,{
    title:'混合测试并且校验数据结构,应该报错',
    req:()=> PostError('/PostTestApi/postParam3',{name:'xiaomin',age:'abc'}),
    assert:({e}) => e.status == 400 && contains(e.responseText,'age','isNumber')
})

doTest(10,{
    title:'混合测试并且校验数据结构',
    req:()=> PostBody('/PostTestApi/postParam4',{name:'xiaomin',age:123}),
    assert:d => d === `xiaomin123`
})


testAll();