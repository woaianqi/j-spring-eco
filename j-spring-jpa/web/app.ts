import { spring } from 'j-spring';
import { springWebModule, MorganLogConfigruation } from 'j-spring-web';
import { mysqlModule } from '../src';
import { TestApiController } from './controller/TestApiController';
import { ZcDataApiController } from './controller/ZcDataApiController';
import { WinstonLog } from 'j-spring-log';
import { UploadPathCollect } from './entity/UploadPathCollect';
import { Image } from './entity/Image';
import { Post } from './entity/Post';

const config = {
  'j-spring': {
    log: {
      level: 'debug,http',
    },
  },
  'j-spring-jpa': {
    host: 'localhost',
    database: 'integration_platform',
    username: 'root',
    password: '12345',
  },
};

spring
  .loadConfig(config)
  .loadLogger(WinstonLog)
  .bindList([MorganLogConfigruation])
  .bindModule([
    mysqlModule([UploadPathCollect, Image, Post]),
    springWebModule([TestApiController, ZcDataApiController]),
  ])
  .invokeStarter();

/***
 *
 * 需要修改的bug
 * springDao的操作 需要设置成代理加上@Service注解，并且方法上加入@Transactional
 *
 *
 */
