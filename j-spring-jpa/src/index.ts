import { SpringStarter } from 'j-spring';
import { SqliteStarter, loadEntity, MysqlStarter } from './Starter';
import { SpringTransictionalPostProcessor } from './SpringJpaPostProcessor';
import { EntityClass } from './SpringJpaEntity';
import { SpringDao } from './SpringRepository';

export * from 'typeorm';
export * from './SpringJpaAnnotation';
export { loadEntity } from './Starter';
export { SpringDao } from './SpringRepository';
export { SpringSearchDao } from './SpringJpaSearch';

export { JpaEntity, JpaSearch } from './SpringJpaEntity';

//传入对应的SpringStater驱动启动器，生成启动模块
export function createModule(starter: new () => SpringStarter) {
  //传入实体类的类名
  return function(entityList: EntityClass[]) {
    loadEntity(entityList);
    return [starter, SpringTransictionalPostProcessor, SpringDao];
  };
}

//sqlite模块
export const sqliteModule = createModule(SqliteStarter);
//mysql模块
export const mysqlModule = createModule(MysqlStarter);

/***
 * 事务模块 设计是错误的
 *  createEntity 本质是从连接池获取链接，不应该无故释放，影响性能！应该重复利用！
 *  springDao在获取entityManager时，是异步的。需要循环检测能够使用的entityManager!
 *  最大数量使用配置的
 *
 *  starter启用公用配置
 *
 *
 */
